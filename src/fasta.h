/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

/*                        PhyloGibbs                                  */

/*   Algorithm developed by Rahul Siddharthan, Erik van Nimwegen      * 
 *   and Eric D. Siggia at The Rockefeller University, New York, USA  *
 *   and at The Institute of Mathematical Sciences, Chennai, India    *
 *                                                                    *
 *   This code copyright (C) 2004-2006 Rahul Siddharthan              *
 *   Licensed under the GNU General Public License (see COPYING)      *
 *   For support and contact information, see the webpage:            *
 *             http://www.imsc.res.in/~rsidd/phylogibbs/              */

/*
 * $Id: fasta.h,v 1.3 2006/03/27 18:44:20 rsidd Exp $
 */
typedef struct {
    GString *header;
    GString *seq;
} fastaseq;
