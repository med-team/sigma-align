/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include "getopt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include "sigma.h"


int parseopts(int argc, char *argv[], params *v)
{

    char Nstr[256],*seqname;
    int n,ch;

    if (argc==0) {
        v->printhelp=1;
        return 0;
    }
    
    
    static struct option longopts[] = {
        {"aligned_output", no_argument, 0, 'A'},
        {"bgseqfile", required_argument, 0, 'b'},
        {"bgprobfile",required_argument, 0, 'B'},
        {"caps_only",no_argument, 0, 'C'},
        {"fasta_output", no_argument, 0, 'F'},
        {"help", no_argument, 0, 'h'},
        {"ncorrel", required_argument, 0, 'n'},
        { "significance",   required_argument,  0,  'x' },
        {0, 0, 0, 0}
    };        
    

    while ((ch = getopt_long(argc,argv,"Ab:B:CFhn:x:", longopts, NULL)) != -1) {
        switch ((char) ch) {
        case 'A':
            n = strlen(v->outtype);
            if (n < 255){
                v->outtype[n]='A';
                v->outtype[n+1]='\0';
            }
            break;
        case 'B': /* name of background prob file name */
            strncpy(v->bgfilename,optarg,255);
            break;
        case 'C':
            v->caps_only=1;
            break;
        case 'F':
            n = strlen(v->outtype);
            if (n < 255){
                v->outtype[n]='F';
                v->outtype[n+1]='\0';
            }
            break;
        case 'b': /* name of bg file */
            strncpy(v->auxfilename,optarg,255);
            break;
        case 'n': /* correlated background */
            strncpy(Nstr,optarg,255);
            v->correlbg = atoi(Nstr);
            if (v->correlbg >2) {
                fprintf(stderr,"Warning: correlation > 2 (dinucleotide) not implemented, using 2\n");
                v->correlbg=2;
            }
            if (v->correlbg < 0) {
                fprintf(stderr,"Error: correlation must be zero or positive\n");
                return 1;
            }
            break;
        case 'x': /* value of significance threshold */
            strncpy(Nstr,optarg,255);
            v->pmatch=atof(Nstr);
            if (v->pmatch < -0.000000000001) { /* disallow negative values */
                fprintf(stderr,"Error: negative values not allowed for pmatch (-x)\n");
                return 1;
            }
            if (v->pmatch > 0.999999999999) {
                fprintf(stderr,"Error: pmatch (-x) must be substantially less than 1\n");
                return 1;
            }
            if (v->pmatch < 0.0)
                v->pmatch = 0.0;
            break;
        default:
            v->printhelp=1;
            break;
        }
    }
    v->filenamelist = g_ptr_array_new();
    while (optind<argc) {
        seqname=malloc(256*sizeof(char));
        strncpy(seqname,argv[optind],255);
        g_ptr_array_add(v->filenamelist,seqname);
        optind++;
    }

    return 0;
}

