/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <stdlib.h>
#include "sigma.h"
#include "commonroutines.h"

void setbgw(params *v, seq_frag *onefrag) 
{
    int n,bn,bn1;

    /* if a base is not ACGT, set bgw as 0.25 */

    onefrag->abgw = malloc(onefrag->seqlen * sizeof(double));
    if (v->correlbg==0) {
        for (n=0; n<onefrag->seqlen; n++)
            onefrag->abgw[n]=0.25;
        return;
    }
    
    bn=base2num(onefrag->aseq[0]);
    if (bn>=0)
        onefrag->abgw[0] = v->singlesite[bn];
    else
        onefrag->abgw[0] = 0.25;
    
    for (n=1; n<onefrag->seqlen; n++) {
        bn=base2num(onefrag->aseq[n]);
        if (bn<0)
            onefrag->abgw[n]=0.25;
        else {
            if ((v->correlbg==1) || ((bn1=base2num(onefrag->aseq[n-1]))<0)) {
                onefrag->abgw[n]=v->singlesite[bn];
            }
            else onefrag->abgw[n] = v->dinucleotide[bn1*4+bn];
        }
    }
}

            
