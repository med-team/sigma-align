/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include "sigma.h"



int is_complete(seq_frag **frag, GPtrArray **nexthead)
     /* if frag is multi-sequence, then
        all copies are at head of seqlist */
{
    int n,ncount=0;

    if ((*frag)==NULL)
        return 0;
    
    for (n=0; n<(*nexthead)->len; n++)
        if (g_ptr_array_index((*nexthead),n)== *frag)
            ncount++;
    assert(ncount <= (*frag)->nseqs);
    
    if (ncount==(*frag)->nseqs)
        return 1;
    else
        return 0;
}


int is_in_frag(int nseq, seq_frag *frag) 
{
    int n;
    for (n=0; n<frag->nseqs; n++)
        if (frag->seqindex[n]==nseq)
            return 1;
    return 0;
}



void add_frag(seq_frag *frag, GPtrArray **nexthead, GPtrArray **outarray, int caps_only)
{
    GString *outseq;
    int lseq = frag->seqlen, pad_needed,n,n1,nseq, npad;

    outseq = g_ptr_array_index((*outarray),frag->seqindex[0]);

    if ((frag->nseqs == 1)&&(caps_only==0)) {
        nseq = frag->seqindex[0];
        for (n=0; n<lseq; n++)
            g_string_append_c(outseq,tolower(frag->seq[nseq][n]));
        g_ptr_array_index((*nexthead),nseq) = frag->neighbours_r[nseq];
        return;
    }

    /* multi-seq frag or caps-only, otherwise we'd have exited */
    npad = outseq->len - 1;    
    do {
        pad_needed=0;
        npad++;
        for (nseq=0; nseq<(*outarray)->len; nseq++) {
            outseq = g_ptr_array_index((*outarray),nseq);
            if (is_in_frag(nseq,frag)) {
                if (npad<outseq->len) {
                    pad_needed = 1;
                    break;
                }   
            }
            else
                for (n=0; n<lseq; n++) {
                    if ((npad+n < outseq->len)
                        &&(isupper(outseq->str[npad+n]))) {
                        pad_needed = 1;
                        break;
                    }
                }
            if (pad_needed)
                break;
        }
    } while (pad_needed);

    for (n=0; n<frag->nseqs; n++) {
        nseq = frag->seqindex[n];
        outseq = g_ptr_array_index((*outarray),nseq);
        while (outseq->len < npad)
            g_string_append_c(outseq,'-');
        for (n1=0; n1<lseq; n1++)
            g_string_append_c(outseq,toupper(frag->seq[nseq][n1]));
        g_ptr_array_index((*nexthead),nseq) = frag->neighbours_r[nseq];        
    }   
}


int remains(GPtrArray **nexthead) 
{
    int n;

    for (n=0; n<(*nexthead)->len; n++)
        if (g_ptr_array_index((*nexthead),n)!=NULL) 
            return 1;
    return 0;
}


void assemble_frags_to_seqs(params *v, GPtrArray **outarray) {
    GPtrArray *nexthead;
    GString *onestring;
    seq_frag *frag;
    int n,maxlen;

    *outarray = g_ptr_array_new();
    nexthead = g_ptr_array_new();
    for (n=0; n<v->nseqs; n++) {
        onestring = g_string_new("");
        g_ptr_array_add(*outarray,onestring);
        frag = g_ptr_array_index(v->seqheadlist,n);
        g_ptr_array_add(nexthead,frag);
    }

    n = 0;

    while (remains(&nexthead)) {
        frag = g_ptr_array_index(nexthead,n);
        if (is_complete(&frag,&nexthead))
            add_frag(frag,&nexthead,outarray,v->caps_only);
        else {
            n++;
            if (n == v->nseqs)
                n=0;
            continue;
        }
    }

    maxlen=0;
    for (n=0; n<(*outarray)->len; n++) {
        onestring = g_ptr_array_index((*outarray),n);
        if ((onestring->len) > maxlen)
            maxlen = onestring->len;
    }
    for (n=0; n<(*outarray)->len; n++) {
        onestring = g_ptr_array_index((*outarray),n);
        while ((onestring->len) < maxlen)
            g_string_append_c(onestring,'-');
    }   
}

        
            

