/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

int base2num(char b);
seq_frag* new_seqfrag(params *v);
