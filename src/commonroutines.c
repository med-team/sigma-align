/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <stdlib.h>
#include "sigma.h"

int base2num(char b) 
{
    switch(b) {
    case 'A': case 'a': return 0; break;
    case 'C': case 'c': return 1; break;
    case 'G': case 'g': return 2; break;
    case 'T': case 't': return 3; break;
    default: return -1;
    }
}

        
seq_frag* new_seqfrag(params *v) 
{
    int n;
    seq_frag *frag;
    
    frag=malloc(sizeof(seq_frag));
    frag->seqindex = malloc(v->nseqs * sizeof(int));
    frag->seq = calloc(v->nseqs, sizeof(char*));
    frag->aseq = NULL;
    frag->abgw = NULL;
    frag->seqlabel = calloc(v->nseqs, sizeof(char*));
    frag->neighbours_l = calloc(v->nseqs, sizeof(seq_frag*));
    frag->neighbours_r = calloc(v->nseqs, sizeof(seq_frag*));
    frag->limits_l = malloc(v->nseqs * sizeof(char*));
    frag->limits_r = malloc(v->nseqs * sizeof(char*));
    for (n=0; n < v->nseqs; n++) {
        frag->limits_l[n] = malloc(2*sizeof(char));
        frag->limits_r[n] = malloc(2*sizeof(char));
        frag->limits_l[n][0] = '0';
        frag->limits_l[n][1] = '\0';        
        frag->limits_r[n][0] = '3';
        frag->limits_r[n][1] = '\0';
    }
    return frag;
}

