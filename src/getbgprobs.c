/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "sigma.h"
#include "commonroutines.h"



int get_count_from_str(char *s, int *nuc1, int *nuc2, double *count) 
{
    /* return code 1 = acceptable error (continue),
       2 = unacceptable error */
    int m=0;

    if (strlen(s)==0)
        return 1;
    if (s[0]=='#')
        return 1;
    if (strlen(s) < 3)
        return 2;
    m=0;
    while ((m<strlen(s))&&(m<255)&&((s[m]==' ')||(s[m]=='\t')))
        m++;
    *nuc1 = base2num(s[m]);
    m++;
    if (*nuc1 < 0)
        return 2;
    if ((s[m]!=' ')&&(s[m]!='\t')) {
        *nuc2 = base2num(s[1]);
        if (*nuc2 < 0)
            return 2;
        m++;
    }
    else *nuc2 = -1;
    while ((m<strlen(s))&&(m<255)&&
           ((s[m]=='A')||(s[m]=='a')||(s[m]=='C')||(s[m]=='c')
            ||(s[m]=='G')||(s[m]=='g')||(s[m]=='T')||(s[m]=='t')))
        m++;
    while ((m<strlen(s))&&(m<255)&&((s[m]==' ')||(s[m]=='\t')))
        m++;
    *count = atof((char*)(s+m));

    return 0;
}

    
int getbgprobs(params *v)  /* return 1 if illegal char in file, 2 if
                            * incomplete file */
{ 
    FILE *probfile;
    char s[256];
    int m,n,nuc1,nuc2,status,dinuc_counted=0;
    double count,stot,dtot;

    probfile=fopen(v->bgfilename,"rt");
    if (probfile==NULL)
        return 1;

    for (m=0; m<4; m++) {
        v->singlesite[m] = -1.0;
        for (n=0; n<4; n++)
            v->dinucleotide[m*4+n] = -1.0;
    }
    

    while (fgets(s,255,probfile)) {
        status = get_count_from_str(s,&nuc1,&nuc2,&count);
        if (status == 1)
            continue;
        if (status)
            return 1;
        if (count <= 0.0)
            return 1;
        if (nuc2 < 0)
            v->singlesite[nuc1] = count;
        else {
            dinuc_counted = 1;
            v->dinucleotide[nuc1*4+nuc2] = count;
        }
    }

    stot=0.0;
    for (m=0; m<4; m++) {
        if (v->singlesite[m]<0.0)
            return 2;
        stot += v->singlesite[m];
        if (dinuc_counted) {
            dtot=0.0;
            if (v->dinucleotide[m*4+n] < 0.0)
                return 2;
            for (n=0; n<4; n++)
                dtot += v->dinucleotide[m*4+n];
            for (n=0; n<4; n++)
                v->dinucleotide[m*4+n] /= dtot;
        }
        else 
            if (v->correlbg == 2) {
                fprintf(stderr,"Warning: dinucleotide counts asked for, but file %s\n         only contains single-nucleotide counts; using uncorrelated background\n\n",v->bgfilename);
                v->correlbg = 1;
            }
    }
    for (m=0; m<4; m++)
        v->singlesite[m] /= stot;

    return 0;
}
