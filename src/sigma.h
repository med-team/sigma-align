/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <glib.h>



typedef struct 
{
    GPtrArray *filenamelist;
    char outtype[255];
    char auxfilename[255];
    char bgfilename[255];
    int correlbg;
    double pmatch;
    int nseqs;
    double singlesite[4];
    double dinucleotide[16];
    int printhelp;
    int caps_only;
    GPtrArray *seqfraglist;
    GPtrArray *seqheadlist;
} params;

typedef struct _seq_frag seq_frag;

struct _seq_frag 
{
    int seqlen;
    int nseqs;
    int swept;
    int *seqindex;
    char **seq;
    char *aseq;
    double *abgw;
    char **seqlabel;
    seq_frag **neighbours_l, **neighbours_r;
    char **limits_l, **limits_r;
};

struct swmatrix /* allocated once and then retained, to save cycles */
{
    double *fmat;
    int *lmat, *mmat;
    int m,n; /* dimensions of matrix */
} swmat;

typedef struct 
{
    seq_frag *frag1, *frag2;
    int n1, n2, l;
    double p;
} match;

