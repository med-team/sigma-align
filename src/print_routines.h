/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

void print_fasta(GArray *seqs, GPtrArray *outarray);
void print_aligned(GArray *seqs, GPtrArray *outarray);
void print_raw(params *v, GArray *seqs);
