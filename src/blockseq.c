/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */
 /* This is the working folder for sigma - Gayathri */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "sigma.h"
#include "fasta.h"
#include "commonroutines.h"

    double *bmfmat;
    int *bmlmat,*bmmmat;

void printn(char *s, int n) 
{
    int m;
    
    for (m=0; m<n; m++)
        printf("%c",s[m]);
}

void printfrags(params *v)  /* debugging routine */
{
    int n,m,p,printed;
    seq_frag *frag;
    
    for (n=0; n<v->nseqs; n++) {
        printf("\nSequence number %d\n",n);
        frag = g_ptr_array_index(v->seqheadlist,n);
        while (frag) {
            printf("   consensus ");
            printn(frag->aseq,frag->seqlen);
            printf("\n");
            for (p=0; p<v->nseqs; p++) {
                printed=0;
                for (m=0; m<frag->nseqs; m++)
                    if (frag->seqindex[m]==p) {
                        printf("   seq %d ",p);
                        printn(frag->seq[p],frag->seqlen);
                        printf("   %s seq %d\n", frag->seqlabel[p],p);
                        printed=1;
                        break;
                    }
                if (printed==0)
                    printf("   pairseq %d limit_l %s limit_r %s\n",
                           p, frag->limits_l[p],frag->limits_r[p]);
            }   
            frag = frag->neighbours_r[n];
        }
        printf("\n\n");
    }
}



void init_sw_matrix(params *v, GArray *seqs) 
{
    int n,i,maxlen=0;
    fastaseq *seq; int j=0;
    
    for (n=0; n<seqs->len; n++) {
        seq= &g_array_index(seqs,fastaseq,n);
        if (seq->seq->len > maxlen)
            maxlen=seq->seq->len;
    }
    swmat.fmat = malloc((maxlen+1)*sizeof(double));
    swmat.lmat = malloc((maxlen+1)*sizeof(int));
    swmat.mmat = malloc((maxlen+1)*sizeof(int));
    for (i=0; i<=maxlen; i++) {
        swmat.fmat[i]=1.0;
        swmat.lmat[i]=0;
        swmat.mmat[i]=0;
    }

    swmat.m = maxlen+1;
    swmat.n = maxlen+1;

bmfmat =calloc (sizeof(double),maxlen+1);
bmlmat = calloc(sizeof(int), maxlen+1);
bmmmat = calloc(sizeof(int), maxlen+1); 
    for (j = 0;j<maxlen;j++)
    {
	bmfmat[j] = 1.0;
	bmlmat[j] = 0;
	bmmmat[j] = 0;
    }	

}



void best_match(params *v, seq_frag *frag1, seq_frag *frag2,
                int *n1, int *n2, int *lbestout, double *bestout)
{
    int sl1, sl2, bestm=0, bestn=0, lbest=0, m, n, m_old, l_new, m_new;
    double best=1.0, f_old, f_new;
    char a1, b1;
    double lfmat = 0.0;
    int i, j =0;
    int llmat = 0, lmmat = 0;

    sl1 = frag1->seqlen;
    sl2 = frag2->seqlen; 
    for (i=0; i<=sl2; i++) {
        swmat.fmat[i]=1.0;
        swmat.lmat[i]=0;
        swmat.mmat[i]=0;
	}
    for (j = 0;j<sl2;j++)
    {
	bmfmat[j] = 1.0;
	bmlmat[j] = 0;
	bmmmat[j] = 0;
	}		
    for (m=1; m<=sl1; m++)
    {
        for (n=1; n<=sl2; n++) {
            a1 = frag1->aseq[m-1];
            b1 = frag2->aseq[n-1];

            f_old = swmat.fmat[n-1];
            m_old = swmat.mmat[n-1];
            l_new = swmat.lmat[n-1] + 1;

            if ((a1==b1)&&(a1 != 'N')) {
                m_new = m_old;
                f_new = f_old * sqrt(frag1->abgw[m-1]*frag2->abgw[n-1]) 
                    * (float) (l_new) / ((float)(l_new - m_old));
                   
            }
            else {
                m_new = m_old + 1;
                f_new = f_old * l_new/(m_old + 1.0);
            }
            if (f_new >= 1.0) 
	    {
                f_new = 1.0;
                l_new = 0;
                m_new = 0;
            }

            bmfmat[n] = f_new; 
            bmlmat[n] = l_new;
            bmmmat[n] = m_new;

            if (f_new < best) {
                best = f_new;
                bestm = m;
                bestn = n;
            }
        } /* end of n loop */

	memcpy(swmat.fmat,bmfmat,sizeof(double)* (sl2+1));
	memcpy(swmat.lmat,bmlmat,sizeof(int)*(sl2+1));
	memcpy(swmat.mmat,bmmmat,sizeof(int)*sl2);
    } /*end of m loop */
 i = bestm; j=bestn;
      if (i>j) 
	{
		i=i-j; j=0;
	}
	else if (j>i)
	{
		j=j-i; i= 0;
}
else
 i=j=0;
    f_old = 1.0; l_new = 1; m_old = 0;
    while ((i< bestm) && (j<bestn))
    {
            a1 = frag1->aseq[i];
            b1 = frag2->aseq[j];
	            if ((a1==b1)&&(a1 != 'N')) {
	                f_new = f_old * sqrt(frag1->abgw[i]*frag2->abgw[j])  
        	            * (float) (l_new) / ((float)(l_new - m_old));
	            }
        	    else {
        	        f_new = f_old * l_new/(m_old + 1.0);
			m_old ++;

		    }
		    if (f_new < 1.0)
		    {
			    lbest++;
		    }
		    else 
		    {
			    lbest = 0; l_new =0; f_new = 1.0; m_old = 0;
		    }
		    i++; j++;f_old = f_new; l_new++;
    } 
    best = best * (sl1-lbest+1) * (sl2-lbest+1);            
    if (best < v->pmatch) {
        *n1 = bestm-lbest;
        *n2 = bestn-lbest;
        *lbestout = lbest;
        *bestout = best;
    }
    else {
        *n1 = 0;
        *n2 = 0;
        *lbestout = 0;
        *bestout = 1.0;
    }
}


void replace_string(char **s1, char **s2, char c) 
{
    int n;

    if (*s1)
        free(*s1);
    n=strlen(*s2);
    if (c=='\0')
        *s1 = calloc(n+1,sizeof(char));
    else
        *s1 = calloc(n+2,sizeof(char));
    strncpy(*s1, *s2, n+1);
    if (c=='\0')
        (*s1)[n]='\0';
    else {
        (*s1)[n]=c;
        (*s1)[n+1]='\0';
    }
}


void reset_swept(params *v) /* avoid sweeping twice from same frag */
{
    seq_frag *frag;
    int n;

    for (n=0; n < v->seqfraglist->len; n++) {
        frag = g_ptr_array_index(v->seqfraglist,n);
        frag->swept = 0;
    }
}


void sweep_limits(char **lim, seq_frag *frag, int ns, int dir) 
{
    int n;

    if ((frag==NULL)||(frag->swept))
        return;

    frag->swept = 1;
    if (dir) { /* left sweep, right lims */
        if (strcmp(frag->limits_r[ns],*lim) > 0) {
            replace_string(&(frag->limits_r[ns]),lim,'\0');
        }
        for (n=0; n<frag->nseqs; n++) 
            sweep_limits(lim,frag->neighbours_l[frag->seqindex[n]],ns,dir);
    }
    else { /* right sweep, left lims */
        if (strcmp(frag->limits_l[ns],*lim) < 0) {
            replace_string(&(frag->limits_l[ns]),lim,'\0');
        }
        for (n=0; n<frag->nseqs; n++)
            sweep_limits(lim,frag->neighbours_r[frag->seqindex[n]],ns,dir);
    }
    
}

            
        



void update_limits(params *v, seq_frag *frag) 
{
    int m,ns,ns2,is_diag;
    seq_frag *fragn;

    for (ns=0; ns <v->nseqs; ns++) {
        is_diag = 0;
        for (m=0; m < frag->nseqs; m++)
            if (frag->seqindex[m]==ns) {
                is_diag = 1;
                break;
            }

        if (is_diag) {
        /* diagonal limits */
            replace_string(&((frag->limits_l)[ns]),&((frag->seqlabel)[ns]),'\0');
            replace_string(&((frag->limits_r)[ns]),&((frag->seqlabel)[ns]),'\0');
        }
        else  /* off-diag */
            /* replace limit for ns with most stringent limit of a
               neighbouring fragment */
            for (m=0; m < frag->nseqs; m++) {
                ns2 = frag->seqindex[m];
                fragn = frag->neighbours_l[ns2];
                if (fragn) {
                    if (strcmp(fragn->limits_l[ns],frag->limits_l[ns])>0)
                        replace_string(&(frag->limits_l[ns]),&(fragn->limits_l[ns]),'\0');
                }
                fragn = frag->neighbours_r[ns2];
                if (fragn) {
                    if (strcmp(fragn->limits_r[ns],frag->limits_r[ns])<0) 
                        replace_string(&(frag->limits_r[ns]),&(fragn->limits_r[ns]),'\0');
                }
            }
    }

    for (ns = 0; ns < v->nseqs; ns++) {
        reset_swept(v);
        sweep_limits(&(frag->limits_l[ns]),frag,ns,0);
        reset_swept(v);
        sweep_limits(&(frag->limits_r[ns]),frag,ns,1);
    }
}

    

int inconsistent_frags(seq_frag *frag1, seq_frag *frag2)
{
    int n1, n2, s1, s2;
    char *label, *lim_l, *lim_r;
    
    for (n1=0; n1< frag1->nseqs; n1++) {
        s1 = frag1->seqindex[n1];
        for (n2=0; n2 < frag2->nseqs; n2++) {
            s2 = frag2->seqindex[n2];
            if (s2 == s1)
                return 1;
            else {
                label = frag2->seqlabel[s2];
                lim_l = frag1->limits_l[s2];
                lim_r = frag1->limits_r[s2];
                if ((strcmp(label,lim_l)<=0)||(strcmp(label,lim_r)>=0))
                    return 1;
                label = frag1->seqlabel[s1];
                lim_l = frag2->limits_l[s1];
                lim_r = frag2->limits_r[s1];
                if ((strcmp(label,lim_l)<=0)||(strcmp(label,lim_r)>=0))
                    return 1;
            }
        }
    }
    return 0;
}


gint compare_matches(gconstpointer *a, gconstpointer *b) 
{
    match *m1, *m2;

    m1 = (match*)(*a);
    m2 = (match*)(*b);
    
    if (m1->p < m2->p)
        return -1;
    else if (m1->p> m2->p)
        return 1;
    else
        return 0;
}


void make_match_list(params *v, GPtrArray **matchlist) 
{
    int n1, n2;
    seq_frag *frag1, *frag2;
    match *onematch;
    
    if (*matchlist) {
        for (n1=0; n1<(*matchlist)->len; n1++) {
            onematch = g_ptr_array_index(*matchlist,n1);
            free(onematch);
        }
        g_ptr_array_free(*matchlist, TRUE);
    }
    
    *matchlist = g_ptr_array_new();
    
    onematch = malloc(sizeof(match));
    
    for (n1=0; n1<v->seqfraglist->len - 1; n1++) {
        frag1 = g_ptr_array_index(v->seqfraglist,n1);
        for (n2 = n1+1; n2 < v->seqfraglist->len; n2++) {
            frag2 = g_ptr_array_index(v->seqfraglist,n2);
            if (inconsistent_frags(frag1,frag2))
                continue;
            best_match(v,frag1,frag2,&(onematch->n1),&(onematch->n2),
                       &(onematch->l),&(onematch->p));
	//printf("\n n1 = %d, n2 = %d, l= %d, p=%f\n", onematch->n1, onematch->n2, onematch->l, onematch->p);
            if (onematch->l > 0) {
                onematch->frag1 = frag1;
                onematch->frag2 = frag2;
                g_ptr_array_add(*matchlist,onematch);
                onematch = malloc(sizeof(match));
            }
        }
    }
    free(onematch);

    g_ptr_array_sort((*matchlist), (GCompareFunc) compare_matches);
}


char consensus(seq_frag *frag, int n) 
{
    int n1,nseq,n2;
    double counts[4];

    counts[0] = 0.0;
    counts[1] = 0.0;
    counts[2] = 0.0;
    counts[3] = 0.0;

    for (n1=0; n1<frag->nseqs; n1++) {
        nseq=frag->seqindex[n1];
        switch(frag->seq[nseq][n]) {
        case 'A': case 'a': counts[0] += 1.0; break;
        case 'C': case 'c': counts[1] += 1.0; break;
        case 'G': case 'g': counts[2] += 1.0; break;
        case 'T': case 't': counts[3] += 1.0; break;
        default: for (n2=0; n2<4; n2++) counts[n2] += 0.25; break;
        }
    }
    if (counts[0] > (frag->nseqs * 0.5))
        return 'A';
    else if (counts[1] > (frag->nseqs * 0.5))
        return 'C';
    else if (counts[2] > (frag->nseqs * 0.5))
        return 'G';
    else if (counts[3] > (frag->nseqs * 0.5))
        return 'T';
    else
        return 'N';
}



void fuse_one_match(params *v, match *onematch) 
{
    int n,ns;
    seq_frag *f1, *f2, *f3, *f4, *f5;
    
    f1 = onematch->frag1;
    f2 = onematch->frag2;

    if (inconsistent_frags(f1,f2))
        return;
    
    f3 = new_seqfrag(v);
    f4 = new_seqfrag(v);
    f5 = new_seqfrag(v);

    /* set seqs of various frags */
    f3->nseqs = f1->nseqs + f2->nseqs;
    f4->nseqs = f1->nseqs;
    f5->nseqs = f2->nseqs;

    f3->seqlen = onematch->l;
    f4->seqlen = f1->seqlen - onematch->n1 - onematch->l;
    f5->seqlen = f2->seqlen - onematch->n2 - onematch->l;
    
    f1->seqlen = onematch->n1;
    f2->seqlen = onematch->n2;

    for (n=0; n<f1->nseqs; n++) {
        ns = f1->seqindex[n];
        f3->seqindex[n] = ns;
        f4->seqindex[n] = ns;
        (f3->seq)[ns] = (char*)((f1->seq)[ns] + onematch->n1);
        (f4->seq)[ns] = (char*)((f1->seq)[ns] + onematch->n1 + onematch->l);

        /* relabel */
        replace_string(&(f3->seqlabel[ns]),&(f1->seqlabel[ns]),'1');
        replace_string(&(f4->seqlabel[ns]),&(f1->seqlabel[ns]),'2');
        /* trickery to relabel first frag */
        replace_string(&(f1->seqlabel[ns]),&(f4->seqlabel[ns]),'\0');
        f1->seqlabel[ns][strlen(f1->seqlabel[ns])-1] = '0';
        /* neighbours */
        f4->neighbours_r[ns] = f1->neighbours_r[ns];
        f1->neighbours_r[ns] = f3;
        f3->neighbours_l[ns] = f1;
        f3->neighbours_r[ns] = f4;
        f4->neighbours_l[ns] = f3;
    }
    for (n=0; n<f2->nseqs;n++) {
        ns = f2->seqindex[n];
        f3->seqindex[n+f1->nseqs] = ns;
        f5->seqindex[n] = ns;
        (f3->seq)[ns] = (char*)((f2->seq)[ns] + onematch->n2);
        (f5->seq)[ns] = (char*)((f2->seq)[ns] + onematch->n2 + onematch->l);
        /* relabel */
        replace_string(&(f3->seqlabel[ns]),&(f2->seqlabel[ns]),'1');
        replace_string(&(f5->seqlabel[ns]),&(f2->seqlabel[ns]),'2');
        /* trickery */
        replace_string(&(f2->seqlabel[ns]),&(f5->seqlabel[ns]),'\0');
        f2->seqlabel[ns][strlen(f2->seqlabel[ns])-1] = '0';
        /* neighbours */
        f5->neighbours_r[ns] = f2->neighbours_r[ns];
        f2->neighbours_r[ns] = f3;
        f3->neighbours_l[ns] = f2;
        f3->neighbours_r[ns] = f5;
        f5->neighbours_l[ns] = f3;
    }
    /* limits */

    for (n=0; n<v->nseqs; n++) {
        replace_string(&(f4->limits_l[n]),&(f1->limits_l[n]),'\0');
        replace_string(&(f5->limits_l[n]),&(f2->limits_l[n]),'\0');
        
        replace_string(&(f4->limits_r[n]),&(f1->limits_r[n]),'\0');
        replace_string(&(f5->limits_r[n]),&(f2->limits_r[n]),'\0');        
    }

    /* aseq, abgw */
    f4->aseq = (char *)(f1->aseq + onematch->n1 + onematch->l);
    f4->abgw = (double *)(f1->abgw + onematch->n1 + onematch->l);    
    f5->aseq = (char *)(f2->aseq + onematch->n2 + onematch->l);    
    f5->abgw = (double *)(f2->abgw + onematch->n2 + onematch->l);
    

    f3->aseq = calloc(f3->seqlen + 1, sizeof(char));
    f3->abgw = calloc(f3->seqlen + 1, sizeof(double));
    for (n=0; n<f3->seqlen; n++) {
        f3->aseq[n] = consensus(f3,n);        
        if (f1->abgw[n+onematch->n1] == f2->abgw[n+onematch->n2])
            f3->abgw[n] = f1->abgw[n+onematch->n1];
        else
            f3->abgw[n] = sqrt((f1->abgw[n+onematch->n1])
                               * (f2->abgw[n+onematch->n2]));
    }
    
    g_ptr_array_add(v->seqfraglist,f3);
    g_ptr_array_add(v->seqfraglist,f4);
    g_ptr_array_add(v->seqfraglist,f5);
    update_limits(v,f3);

//    printf("\n--------------------------------------\n");
//    printfrags(v);
//    fflush(NULL);
    
}


int seqindices_match(seq_frag *frag1, seq_frag *frag2) 
{
    int n1,n2, is_in;
    if (frag1->nseqs != frag2->nseqs)
        return 0;
    
    for (n1=0; n1<frag1->nseqs; n1++) {
        is_in = 0;
        for (n2=0; n2<frag2->nseqs; n2++) 
            if (frag1->seqindex[n1]==frag2->seqindex[n2]) {
                is_in=1;
                break;
            }
        if (is_in==0)
            return 0;
    }
    return 1;
}

            


void expand_match(match *onematch, GPtrArray **outmatches) 
{
    /* In case of earlier fusions, a match may now be split over
       multiple fragments; recover from this situation */
    match *tmpmatch, *nextmatch;
    seq_frag *nextfrag1, *nextfrag2;
    int  s1,s2, n1, n2;
    
    *outmatches = g_ptr_array_new();
    tmpmatch=malloc(sizeof(match));
    tmpmatch->frag1 = onematch->frag1;
    tmpmatch->frag2 = onematch->frag2;
    tmpmatch->n1 = onematch->n1;
    tmpmatch->n2 = onematch->n2;
    tmpmatch->l = onematch->l;
    tmpmatch->p = onematch->p;
    s1 = tmpmatch->frag1->seqindex[0];
    s2 = tmpmatch->frag2->seqindex[0];
    while ((tmpmatch->n1 + tmpmatch->l > tmpmatch->frag1->seqlen)||
           (tmpmatch->n2 + tmpmatch->l > tmpmatch->frag2->seqlen)) {
        n1=0;
        while ((tmpmatch->frag1->seqindex[n1] != s1)
               && (n1 < tmpmatch->frag1->nseqs))
            n1++;
        assert(n1<tmpmatch->frag1->nseqs);
        nextfrag1 = tmpmatch->frag1->neighbours_r[s1];
        n2=0;
        while ((tmpmatch->frag2->seqindex[n2] != s2)
               && (n2 < tmpmatch->frag2->nseqs))
            n2++;
        assert(n2 < tmpmatch->frag2->nseqs);
        nextfrag2 = tmpmatch->frag2->neighbours_r[s2];
        if ((tmpmatch->frag1->seqlen <= tmpmatch->n1)
            &&(tmpmatch->frag2->seqlen <= tmpmatch->n2)) {
            /* match lies outside both current frags */
            assert(nextfrag1 != NULL);
            assert(nextfrag2 != NULL);
            tmpmatch->n1 -= tmpmatch->frag1->seqlen;
            tmpmatch->n2 -= tmpmatch->frag2->seqlen;
            tmpmatch->frag1 = nextfrag1;
            tmpmatch->frag2 = nextfrag2;
        }
        else if (tmpmatch->frag1->seqlen <= tmpmatch->n1) {
            /* only frag1 is outside bounds */
            assert(nextfrag1 != NULL);
            tmpmatch->n1 -= tmpmatch->frag1->seqlen;
            tmpmatch->frag1 = nextfrag1;
        }
        else if (tmpmatch->frag2->seqlen <= tmpmatch->n2) {
            assert(nextfrag2!=NULL);
            tmpmatch->n2 -= tmpmatch->frag2->seqlen;
            tmpmatch->frag2 = nextfrag2;
        }
        else { /* Both frags contain some alignable sequence;
                  we align what exists and continue */
            nextmatch = malloc(sizeof(match));
            nextmatch->frag1 = tmpmatch->frag1;
            nextmatch->frag2 = tmpmatch->frag2;
            nextmatch->p = tmpmatch->p;
            if ((tmpmatch->frag1->seqlen - tmpmatch->n1)
                < (tmpmatch->frag2->seqlen - tmpmatch->n2)) {
                /* frag1 contains less alignable sequence than frag2 */
                nextmatch->frag1 = nextfrag1;
                nextmatch->n1 = 0;
                nextmatch->l = tmpmatch->n1 + tmpmatch->l
                    - tmpmatch->frag1->seqlen;
                nextmatch->n2 = tmpmatch->n2 + tmpmatch->l - nextmatch->l;
                tmpmatch->l = tmpmatch->frag1->seqlen - tmpmatch->n1;
            }
            else { /* frag2 contains less alignable seq than frag1 */
                nextmatch->frag2 = nextfrag2;
                nextmatch->n2 = 0;
                nextmatch->l = tmpmatch->n2 + tmpmatch->l
                    - tmpmatch->frag2->seqlen;
                nextmatch->n1 = tmpmatch->n1 + tmpmatch->l - nextmatch->l;
                tmpmatch->l = tmpmatch->frag2->seqlen - tmpmatch->n2;
            }
            g_ptr_array_add(*outmatches,tmpmatch);
            tmpmatch=nextmatch;
        }
    }
    g_ptr_array_add(*outmatches,tmpmatch);
    return;
}



int fuse_match_list(params *v, GPtrArray *matchlist) 
{
    int m,n,aligned=0;
    match *onematch, *tmpmatch;
    GPtrArray *outmatches;
    
    /* assume matchlist is sorted, carry out fusions */
    for (m=0; m<matchlist->len; m++) {
        onematch = g_ptr_array_index(matchlist,m);
        expand_match(onematch, &outmatches);
        if (outmatches) {
            for (n=0; n<outmatches->len; n++) {
                tmpmatch=g_ptr_array_index(outmatches,n);
                if (inconsistent_frags(tmpmatch->frag1,tmpmatch->frag2))
                    break;
                fuse_one_match(v,tmpmatch);
                aligned=1;
            }
            for (n=0; n<outmatches->len; n++) {
                tmpmatch = g_ptr_array_index(outmatches,n);
                free(tmpmatch);
            }
            g_ptr_array_free(outmatches,TRUE);
        }
    }
    return aligned;
}






void blockseq(params *v)
{
    int aligned;
    GPtrArray *matchlist;

    matchlist = NULL;

    do {
        aligned=0;
        make_match_list(v,&matchlist);
        if (matchlist->len > 0) {
            aligned=fuse_match_list(v,matchlist);
        }
    } while (aligned);
}
