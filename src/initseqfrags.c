/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <stdlib.h>
#include <string.h>
#include "sigma.h"
#include "fasta.h"
#include "setbgw.h"
#include "commonroutines.h"

void initseqfrags(params *v, GArray *seqs)
{
    int n,len,len1;
    fastaseq *oneseq;
    seq_frag *onefrag;

    v->seqfraglist = g_ptr_array_new();
    v->seqheadlist = g_ptr_array_new();

    for (n=0; n<seqs->len; n++) {
        oneseq= &g_array_index(seqs,fastaseq,n);

        len = (oneseq->seq->len);


        onefrag = new_seqfrag(v);
        onefrag->seqlen = len;
        
        onefrag->nseqs=1;
        onefrag->seqindex[0] = n;        

        onefrag->seq[n] = (oneseq->seq->str);

        onefrag->aseq = malloc(sizeof(char)*(onefrag->seqlen + 1));
        strncpy(onefrag->aseq,onefrag->seq[n],onefrag->seqlen);

        setbgw(v,onefrag);

        onefrag->seqlabel[n] = malloc((4)*sizeof(char));
        onefrag->seqlabel[n][0] = '0';
        onefrag->seqlabel[n][1] = '.';
        onefrag->seqlabel[n][2] = '1';
        onefrag->seqlabel[n][3] = '\0';

        onefrag->neighbours_l[n] = NULL;
        onefrag->neighbours_r[n] = NULL;
        
        g_ptr_array_add((v->seqfraglist),onefrag);
        g_ptr_array_add((v->seqheadlist),onefrag);
        
    }
}

