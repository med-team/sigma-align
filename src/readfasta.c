/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

/* This routine was originally written for PhyloGibbs  */
/* Copyright (C) 2004-2006 Rahul Siddharthan */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <glib.h>
#include <assert.h>
#include "fasta.h"

void chomp(char *s) {
    if (s[strlen(s)-1]=='\n')
        s[strlen(s)-1]='\0';
    if (s[strlen(s)-1]=='\r')
        s[strlen(s)-1]='\0';
}

int readfasta(const char *filename, GArray **fastaseqs) {

    FILE *seqfile;
    fastaseq oneseq;
    int headernext;
    char s[131072];

    headernext=1;
    if (*fastaseqs == NULL)
        *fastaseqs =g_array_new(TRUE,TRUE,sizeof(fastaseq));

    oneseq.seq=g_string_new("");

    seqfile=fopen(filename,"rt");
    if (seqfile==NULL)
        return 1;
    while (fgets(s,131072,seqfile)) {
        while ((isspace(s[0]))||(s[0]==';')||(s[0]=='#')) {
            if (!fgets(s,131072,seqfile)) {
                if (!headernext)  {
                    if (oneseq.seq->len==0) {
                        fclose(seqfile);
                        return 1;
                    } else {
                        g_array_append_val((*fastaseqs),oneseq);
                        fclose(seqfile);
                        return 0;
                    }
                }
                else {
                    fclose(seqfile);
                    return 0;
                }
            }
            if (oneseq.seq->len > 0) {
                g_array_append_val((*fastaseqs),oneseq);
                oneseq.seq=g_string_new("");
                headernext=1;
            }
        }

        if ((s[0]!='>')&&headernext) {
            fclose(seqfile);
            return 1;
        }

        if (headernext) {
            chomp(s);
            oneseq.header=g_string_new(s+1);
            headernext=0;
        } else {
            chomp(s);
            if (s[0]=='>') {
                if (oneseq.seq->len==0){
                    g_string_free(oneseq.header, TRUE);
                    oneseq.header=g_string_new(s+1);
                }
                else {
                    g_array_append_val((*fastaseqs),oneseq);
                    oneseq.header=g_string_new(s+1);
                    oneseq.seq=g_string_new("");
                }
            } else
                oneseq.seq=g_string_append(oneseq.seq, s);
        }

    }
    if (!headernext) 
        g_array_append_val((*fastaseqs),oneseq);
    
    fclose(seqfile);
    return 0;
}

