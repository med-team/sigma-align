/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sigma.h"
#include "fasta.h"
#include "readfasta.h"
#include "printhelp.h"
#include "parseopts.h"
#include "getbgprobs.h"
#include "setbgprobs.h"
#include "initseqfrags.h"
#include "blockseq.h"
#include "assemble_frags_to_seqs.h"
#include "print_routines.h"

int main(int argc, char *argv[]) 
{
    int n;
    params v;
    GArray *seqs, *bgseqs;
    GPtrArray *outarray;

    seqs=NULL;
    bgseqs=NULL;
    
    v.filenamelist=NULL;
    v.outtype[0]='\0';
    v.auxfilename[0]='\0';
    v.bgfilename[0]='\0';
    v.correlbg=2;
    v.pmatch=0.002;
    v.printhelp=0;
    v.caps_only=0;
    
    v.seqfraglist=NULL;
    v.seqheadlist=NULL;

    if (parseopts(argc,argv,&v))
        exit(1);

    if (v.printhelp) {
        printhelp();
        exit(0);
    }

    if (v.filenamelist->len==0) {
        fprintf(stderr,"Error: must specify files to be aligned, in fasta format\n\n");
        fflush(NULL);
        printhelp();
        exit(1);
    }

    if (strlen(v.outtype)==0) {
        v.outtype[0]='A';
        v.outtype[1]='\0';
    }

    for (n=0; n<v.filenamelist->len; n++) {
        if (readfasta((char*)g_ptr_array_index(v.filenamelist,n),&seqs))
            fprintf(stderr,"Error reading from file %s\n",(char *)g_ptr_array_index(v.filenamelist,n));
    }

    if (seqs->len ==0 ) {
        fprintf(stderr,"Error: no sequences read (check filename and file format): exiting\n");
        exit(1);
    }
    

    if (seqs->len == 1 ) {
        fprintf(stderr,"Error: only one sequence read successfully, nothing to be done\n");
        exit(1);
    }

    v.nseqs=seqs->len;

    if (strlen(v.auxfilename)>0) {
        if (readfasta(v.auxfilename,&bgseqs)) {
            fprintf(stderr,"Error reading background sequence file: %d sequences read\n",bgseqs->len);
            if (bgseqs->len==0) {
                g_array_free(bgseqs,TRUE);
                bgseqs=NULL;
            }
        }
    }
    if (((bgseqs==NULL)||(bgseqs->len==0)) && (strlen(v.bgfilename)>0)) {
        if (getbgprobs(&v)) {
            fprintf(stderr, "Error reading bgcounts from file %s; using input sequence\n",v.bgfilename);
            setbgprobs(&v,seqs);
        }
    }
    else if ((bgseqs!=NULL)&&(bgseqs->len>0))
        setbgprobs(&v,bgseqs);
    else
        setbgprobs(&v,seqs);

    initseqfrags(&v,seqs);

    init_sw_matrix(&v,seqs);
    
    /* if we have got here, we must be all set to do the alignment */

    blockseq(&v);

    assemble_frags_to_seqs(&v,&outarray);


//    print_raw(&v,seqs);
    for (n=0; n<strlen(v.outtype); n++) {
        if (v.outtype[n]=='A')
            print_aligned(seqs,outarray);
        else
            print_fasta(seqs,outarray);
    }
    return 0;
}

