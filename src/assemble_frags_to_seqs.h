/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

void assemble_frags_to_seqs(params *v, GPtrArray **outarray);
