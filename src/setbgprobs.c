/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include "sigma.h"
#include "fasta.h"
#include "commonroutines.h"

int setbgprobs(params *v, GArray *bgseqs) 
{
    int i,p,m,n;
    fastaseq *fseq;
    double stot,dtot;
    
    
    for (m=0; m<4; m++) {
        v->singlesite[m]=0.0;
        for (n=0; n<4; n++)
            v->dinucleotide[m*4+n]=0.0;
    }

    for (i=0; i<bgseqs->len; i++) {
        fseq = &g_array_index(bgseqs,fastaseq,i);
        for (p=0; p<fseq->seq->len-1; p++) {
            if (base2num(fseq->seq->str[p])<0)
                continue;
            v->singlesite[base2num(fseq->seq->str[p])] += 1.0;
            if (base2num(fseq->seq->str[p+1])<0)
                continue;
            v->dinucleotide[base2num(fseq->seq->str[p])*4
                            + base2num(fseq->seq->str[p+1])] += 1.0;
        }
    }

    stot=0.0;
    for (m=0; m<4; m++) {
        stot += v->singlesite[m];
        dtot=0.0;
        for (n=0; n<4; n++)
            dtot += v->dinucleotide[m*4+n];
        for (n=0; n<4; n++)
            v->dinucleotide[m*4+n] /= dtot;
    }
    for (m=0; m<4; m++)
        v->singlesite[m] /= stot;

    return 0;
}
