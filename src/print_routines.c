/* Sigma - Simple greedy multiple alignment */
/* Copyright (C) 2007 Rahul Siddharthan     */
/* Licensed under the GNU GPL, version 2    */
/* See file COPYING in source distribution  */

#include <stdio.h>
#include <stdlib.h>
#include "sigma.h"
#include "fasta.h"

void print_fasta(GArray *seqs, GPtrArray *outarray) 
{
    GString *onestring;
    fastaseq *oneseq;
    int n;

    for (n=0; n<seqs->len; n++) {
        oneseq = &g_array_index(seqs,fastaseq,n);
        onestring = g_ptr_array_index(outarray,n);
        printf(">%s\n",oneseq->header->str);
        printf("%s\n",onestring->str);
    }
    printf("\n");
}

void print_raw(params *v, GArray *seqs) 
{
    fastaseq *oneseq;
    seq_frag *onefrag;
    int n,m,k;

    for (n=0; n<v->nseqs; n++) {
        oneseq = &g_array_index(seqs,fastaseq,n);
        printf(">%s\n",oneseq->header->str);
        onefrag=g_ptr_array_index(v->seqheadlist,n);
        while (onefrag != NULL) {
            m=0;
            while ((onefrag->seqindex[m])!=n)
                m++;
            for (k=0; k<onefrag->seqlen; k++)
                printf("%c",onefrag->seq[m][k]);
            onefrag = onefrag->neighbours_r[m];
        }
        printf("\n");
    }
}

    
void print_aligned(GArray *seqs, GPtrArray *outarray)
{
    GString *onestring;
    fastaseq *oneseq;
    int n,m,ctr,maxlen,*currlens;
    char **outheads;

    onestring = g_ptr_array_index(outarray,0);
    maxlen = onestring->len;
    currlens = calloc(sizeof(int),outarray->len);

    outheads = calloc(sizeof(char*),seqs->len);
    for (n=0; n<seqs->len; n++) {
        outheads[n] = calloc(sizeof(char),18);
        oneseq = &g_array_index(seqs,fastaseq,n);
        m=0;
        while ((m < 15)&&(m<oneseq->header->len)) {
            outheads[n][m] = oneseq->header->str[m];
            m++;
        }
        while (m<17) {
            outheads[n][m] = ' ';
            m++;
        }
        outheads[n][17]='\0';
    }

    ctr = 0;
    while (ctr < maxlen) {
        for (n=0; n<seqs->len; n++) {
            onestring = g_ptr_array_index(outarray,n);            
            printf("%17s ",outheads[n]);
            for (m=0; m<50; m++) {
                if (ctr+m<onestring->len){
                    printf("%c",onestring->str[m+ctr]);
                    if (onestring->str[m+ctr]!='-')
                        currlens[n]++;
                }
                else
                    printf(" ");
                if ((m+1)%10 == 0)
                    printf(" ");
            }
            printf(" %5d\n",currlens[n]);
        }
        printf("\n");
        ctr += 50;
    }
    printf("\n");
    free(currlens);
    for (n=0; n<seqs->len; n++) 
        free(outheads[n]);
    free(outheads);
}
