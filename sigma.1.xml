<?xml version='1.0' encoding='ISO-8859-1'?>
<?xml-stylesheet type="text/xsl"
        href="http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN"
        "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [

  <!ENTITY dhfirstname "Charles">
  <!ENTITY dhsurname   "Plessy">
  <!ENTITY dhdate      "2007-04-07">
  <!ENTITY dhsection   "1">
  <!ENTITY dhemail     "charles-debian-nospam@plessy.org">
  <!ENTITY dhusername  "&dhfirstname; &dhsurname;">
  <!ENTITY dhucpackage "SIGMA">
  <!ENTITY dhpackage   "sigma">
  <!ENTITY dhrelease   "1.1">
  <!ENTITY dhtitle     "Manual of Sigma">

]>

<refentry>
  <refentryinfo>
    <title>&dhtitle;</title>
    <productname>&dhpackage;</productname>
    <releaseinfo role="version">&dhrelease;</releaseinfo>
    <date>&dhdate;</date>
    <authorgroup>
      <author>
        <firstname>Rahul</firstname>
        <surname>Siddharthan</surname>
        <contrib>
          Wrote sigma. If you're using Sigma for actual research, please let the author know so that he can alert you of bugfixes or new releases.
        </contrib>
        <address>
          <affiliation>
            <orgname>
              The Institute of Mathematical Sciences
            </orgname>
            <address>
              <city>Chennai</city>
              <country>India</country>
            </address>
          </affiliation>
          <email>rsidd@imsc.res.in</email>
        </address>
      </author>
      
      <author>
        <firstname>&dhfirstname;</firstname>
        <surname>&dhsurname;</surname>
        <contrib>
          Wrote the manpage in DocBook XML for the Debian distribution.
        </contrib>
        <address>
          <email>&dhemail;</email>
        </address>
      </author>
    </authorgroup>
    
    <copyright>
      <year>2006</year>
      <year>2007</year>
      <holder>Rahul Siddharthan</holder>
    </copyright>
    
    <copyright>
      <year>2006</year>
      <year>2007</year>
      <holder>&dhusername;</holder>
    </copyright>
    
    <legalnotice>
      <para>
        Sigma is free software. You can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation.
      </para>
      
      <para>
        On Debian systems, the complete text of the GNU General Public License can be found in /usr/share/common-licenses/GPL.
      </para>
    </legalnotice>
  </refentryinfo>
  
  <refmeta>
    <refentrytitle>&dhucpackage;</refentrytitle>
    <manvolnum>&dhsection;</manvolnum>
  </refmeta>
  
  <refnamediv>
    <refname>&dhpackage;</refname>
    <refpurpose>
      Simple greedy multiple alignment of non-coding DNA sequences
    </refpurpose>
  </refnamediv>
  
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg choice="opt">options</arg>
      <arg>
        <replaceable>inputfile.fasta</replaceable>
      </arg>
      <arg choice="opt">
        <replaceable>inputfile2.fasta ...</replaceable>
      </arg>
    </cmdsynopsis>
   
    <para>
      Each fasta file may contain a single sequence or multiple sequences; all sequences will be aligned together.
    </para>
    
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para><command>Sigma</command> ("Simple greedy multiple alignment") is an alignment program with a new algorithm and scoring scheme designed specifically for non-coding DNA sequence. It uses a strategy of seeking the best possible gapless local alignments, at each step making the best possible alignment consistent with existing alignments, and scores the significance of the alignment based on the lengths of the aligned fragments and a background model which may be supplied or estimated from an auxiliary file of intergenic DNA. With real data, while "correctness" can't be directly quantified for the alignment, running the PhyloGibbs motif finder on pre-aligned sequence suggests that Sigma's alignments are superior.</para>
    
  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <variablelist>
      <varlistentry>
        <term>
          <option>-A</option>
          <option>--aligned_output</option>
        </term>
        <listitem>
          <para>Aligned, pretty-printed output (compare
          with <option>-F</option> option) (default: only this).  See
          also <option>-C</option>.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry>
        <term>
          <option>-b</option>
          <option>--bgprobfile</option>
          <parameter>filename</parameter>
        </term>
        <listitem>
          <para>Auxiliary file (in fasta format) from which to read
          background sequences (overridden by <option>-B</option>).
          Typically this is a file containing large quantities of
          similar non-coding sequence, from which background
          probabilities of single- and di-nucleotides may be estimated.</para>
        </listitem>
      </varlistentry>
      
      <varlistentry>
        <term>
          <option>-B</option>
          <option>--bgseqfile</option>
          <parameter>filename</parameter>
        </term>
        <listitem>
          <para>File containing background probabilities. The format
            is described further below.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>
          <option>-C</option>
          <option>--caps_only</option>
        </term>
        <listitem>
          <para>Use only upper-case letters in output sequence, for compatibility with output of some other programs like ClustalW and MLagan.  By default, output is mixed-case (as in Dialign), and lower-case bases are treated as not aligned.
          </para>
        </listitem>
      </varlistentry>

     <varlistentry>
        <term>
          <option>-F</option>
          <option>--fasta_output</option>
        </term>
        <listitem>
          <para>Multi-fasta output (can use both <option>-A</option>
          and <option>-F</option> in either order).  See also <option>-C</option>.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>
          <option>-n</option>
          <option>--ncorrel</option>
          <parameter>number</parameter>
        </term>
        <listitem>
          <para>
            Background correlation (default <parameter>2</parameter>=dinucleotide; <parameter>1</parameter>=single-site basecounts, <parameter>0</parameter>=0.25 per base).
          </para>
        </listitem>
      </varlistentry>
            
      <varlistentry>
        <term><option>-x</option>, <option>--significance</option>
          <parameter>number</parameter>
        </term>
        <listitem>
          <para>Set limit for how probable the match is by chance
     (default 0.002, smaller=more stringent).</para>
        </listitem>
      </varlistentry>
      
      <varlistentry>
        <term><option>-h</option>,
              <option>--help</option>
        </term>
        <listitem>
          <para>Displays this list of options.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  
  <refsect1>
    <title>MORE HELP</title>
    <para>
      The "significance" parameter (<option>-x</option>) determines whether local alignments are accepted or rejected. The default at present is 0.002. Experiments on synthetic data (described in the paper) suggest that 0.002 is about the threshold where sigma fails to align phylogenetically-unrelated data that has moderate (yeast-like) dinucleotide correlation.
    </para>
    <para>
      Using a <quote>background model</quote> appropriate to the sequences being aligned greatly reduces spurious alignments on synthetic data (and, one hopes, on real data too). The simplest way to ensure this is to supply, via the <option>-b</option> parameter, a FASTA-format file containing large quantities of similar sequence data (eg, if one is aligning yeast sequences, supply a file containing all intergenic yeast sequence).
    </para>
    <para>
      Instead of this, if the single-site and dinucleotide frequencies are known already, they may be supplied in a file via the <option>-B</option> option. The file format should be: one entry per line, with the mononucleotide or dinucleotide (case-insensitive) followed by the frequency. (eg, "A 0.3", "AT 0.16", etc on successive lines.) A sample file is in the "Background" subdirectory of the source distribution (on Debian systems, this file can be found in the <filename>/usr/share/doc/sigma-align/Background</filename> directory). A file like "yeast.nc.3.freq" in the "tests" subdirectory of the MEME source distribution works fine (trinucleotide counts are ignored).
    </para>
  </refsect1>
  
  <refsect1>
    <title>REFERENCE</title>
    <para>
      Please cite Sigma: Rahul Siddharthan (2006) Multiple alignment of weakly-conserved non-coding DNA sequence BMC Bioinformatics 2006, 7:143     doi:10.1186/1471-2105-7-143 Published 16 March 2006, available online at http://www.biomedcentral.com/1471-2105/7/143/</para>
  </refsect1>
</refentry>
